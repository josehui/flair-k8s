# Flair KVM and K8s setup

## Intro

This repo contains scripts and configuration files for setting up Flair KVM/ K8s environment

## Structure (TBC)

```
├── K8s #
├── KVM # Ansible files for bootstraping KVM host
└── README.md

```

## Usage

### Bootstrap KVM host

> Remember to first create Virtual Disk for KVM data using iDRAC

1. Install Ansible on KVM host or workstation (kvmhost1 is used atm)

   ```shell
   $ sudo apt update
   $ sudo apt install software-properties-common
   $ sudo add-apt-repository --yes --update ppa:ansible/ansible
   $ sudo apt install ansible
   ```

2. (Optional) Edit host vairables  
   E.g. Change storage device name (_default is /dev/sdb_)

- group_vars/kvmhost.yml - variables for all hosts
- host_vars/<hostname.yml> - variables for single host

3. Run playbook with kvmhost role  
    _Run on all hosts_
   ```shell
   $ cd KVM
   $ ansible-playbook -i hosts --private-key <admin_ssh_key> main.yml
   ```
   _Run on single host (e.g. kvmhost2)_
   ```shell
   $ cd KVM
   $ ansible-playbook -i hosts --private-key <admin_ssh_key> main.yml --limit kvmhost2
   ```
   _Additional flags_
   ```
   --check // dry-run
   --kK // run with ssh password instead (requires sshpass)
   ```
   **Storage pool may not be ready when running playbook for first time**
   ```shell
   The conditional check 'ansible_libvirt_pools[\"{{storage_pool_name}}\"].state != \"active\"' failed
   ```
   In that case, a simple re-run should solve the issue

## TO-DO

1. Group ansible tasks by role/ block
1. Create VM images for worker nodes (Packer), e.g.
   - Include Nvidia driver
   - Configure static ip
1. Provision worker nodes + GPU license server (Terraform/ Cloud-init)
1. Deploy K8s cluster (Kubespray)
1. Configure K8s resources, e.g.
   - Dashboard
   - MetalLB
   - RBAC

## Possible Improvements

1. Group ansible tasks by role/ block
